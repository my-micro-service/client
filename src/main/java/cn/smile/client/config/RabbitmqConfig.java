package cn.smile.client.config;

import cn.smile.commons.constant.RabbitMqConstant;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RabbitMQ配置类
 *
 * @author smile
 */
@Configuration
public class RabbitmqConfig {

    /**
     * 声明交换机
     *
     * @return 消息队列交换机
     */
    @Bean(RabbitMqConstant.EXCHANGE_TOPICS_INFORM)
    public Exchange exchangeTopicsInform() {
        //durable(true) 持久化，mq重启之后交换机还在
        return ExchangeBuilder.topicExchange(RabbitMqConstant.EXCHANGE_TOPICS_INFORM).durable(true).build();
    }

    /**
     * 声明QUEUE_INFORM_EMAIL队列
     *
     * @return 队列
     */
    @Bean(RabbitMqConstant.QUEUE_INFORM_EMAIL)
    public Queue queueInformEmail() {
        return new Queue(RabbitMqConstant.QUEUE_INFORM_EMAIL);
    }

    /**
     * 声明QUEUE_INFORM_SMS队列
     *
     * @return 队列
     */
    @Bean(RabbitMqConstant.QUEUE_INFORM_SMS)
    public Queue queueInformSms() {
        return new Queue(RabbitMqConstant.QUEUE_INFORM_SMS);
    }

    /**
     * ROUTING_KEY_EMAIL队列绑定交换机，指定routingKey
     *
     * @param queue    消息队列
     * @param exchange 消息交换机
     * @return 结果
     */
    @Bean
    public Binding bindingQueueInformEmail(@Qualifier(RabbitMqConstant.QUEUE_INFORM_EMAIL) Queue queue,
                                           @Qualifier(RabbitMqConstant.EXCHANGE_TOPICS_INFORM) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(RabbitMqConstant.ROUTING_KEY_EMAIL).noargs();
    }

    /**
     * ROUTING_KEY_SMS队列绑定交换机，指定routingKey
     *
     * @param queue    消息队列
     * @param exchange 消息交换机
     * @return 结果
     */
    @Bean
    public Binding bindingRoutingKeySms(@Qualifier(RabbitMqConstant.QUEUE_INFORM_SMS) Queue queue,
                                        @Qualifier(RabbitMqConstant.EXCHANGE_TOPICS_INFORM) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(RabbitMqConstant.ROUTING_KEY_SMS).noargs();
    }
}
