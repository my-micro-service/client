package cn.smile.client.service;

/**
 * @author smile
 */
public interface RabbitMqService {

    /**
     * 发送普通消息
     * @param o 消息体对象
     */
    void sendMessage(Object o);

    /**
     * 发送email消息
     * @param o 消息对象
     */
    void sendEmail(Object o);
}
