package cn.smile.client.service.impl;

import cn.smile.client.config.RabbitmqConfig;
import cn.smile.client.service.RabbitMqService;
import cn.smile.commons.constant.RabbitMqConstant;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author smile
 */
@Service
public class RabbitMqServiceImpl implements RabbitMqService {

    @Resource
    private RabbitTemplate template;

    @Override
    public void sendMessage(Object o) {
        template.convertAndSend(RabbitMqConstant.EXCHANGE_TOPICS_INFORM, RabbitMqConstant.ROUTING_KEY_SMS_STRING, o);
    }

    @Override
    public void sendEmail(Object o) {
        template.convertAndSend(RabbitMqConstant.EXCHANGE_TOPICS_INFORM, RabbitMqConstant.ROUTING_KEY_EMAIL_STRING, o);
    }
}
